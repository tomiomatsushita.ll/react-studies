import {unstable_useTransition} from "react";
import Spinner from "./Spinner";

export default function buttonPending ({children, onClick, ...props}) {
  const [startTransition, isPending] = unstable_useTransition ({
    tomeoutMs:3000
  });

  function handleClick () {
    startTransition(onClick);
  }

  return(
    <button onClick={handleClick} {...props}>
    {isPending && <Spinner/>}
    {children}
    {isPending && <Spinner/>}
    </button>
  );
}